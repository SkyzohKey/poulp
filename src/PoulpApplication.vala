/**
* TODO: Licensing the code.
**/
using Json;
using Poulp.Modules;

namespace Poulp {
  public class PoulpAction {
    public static const string INIT = "init";
    public static const string BUILD = "build";
    public static const string INSTALL = "install";
    public static const string HELP = "help";
  }

  public class PoulpApplication : Application {
    public static const string generated_main_file = """using GLib;

public static int main (string[] args) {
  stdout.printf ("Hello World from """ + Constants.APP_NAME + """ version """ + Constants.APP_VERSION + """!");
  return 0;
}
""";

    /**
    * The current working directory in which the specified action should work.
    **/
    private string working_dir { get; set; default = null; }
    private string[] arguments { get; set; default = {}; }
    private string action { get; set; default = null; }
    private IModule module { get; set; default = null; }

    public PoulpApplication () {
      GLib.Object (application_id: "org.bat.poulp", flags: ApplicationFlags.HANDLES_COMMAND_LINE);
      
      Logger.logc (LogColor.CYAN, "~~ %s version %s: A minimal Vala project manager. ~~ \n", Constants.APP_NAME, Constants.APP_VERSION);
      this.perform_poulp_checks ();
    }

    public override void activate () {
      this.hold ();
      this.release ();
    }

    public override int command_line (ApplicationCommandLine cmd) {
      this.hold ();

      this.working_dir = cmd.get_cwd ();
      this.arguments   = cmd.get_arguments ();
      this.action      = this.arguments[1];

      this.dispatch_actions ();

      this.release ();
      return 0;
    }
    
    /**
    * This method is intended to create needed folders for poulp (cache, projects templates, etc).
    * It also aims to initialize everything the app will need to perform the user asked action.
    **/
    private void perform_poulp_checks () {
      /**
      * TODO: Check if a previous poulp version has created files.
      * TODO: Ask the user if it wants to clean olders cache (if previous version was here).
      * TODO: Verify if there are poulp update, if so warn the user.
      * TODO: If files/folders are needed by poulp, create them and set correct permissions on them.
      **/
    }

    private void dispatch_actions () {
      switch (this.action) {
        case PoulpAction.INIT:
          this.module = new InitModule (this.arguments, this.working_dir);
          //this.action_init ();
          break;
        case PoulpAction.BUILD:
          this.action_build ();
          break;
        case PoulpAction.INSTALL:
          this.action_install ();
          break;
        case PoulpAction.HELP:
          this.action_help ();
          break;
        default:
          this.action_usage ();
          break;
      }
    }

    private void action_init () {
      /**
      * TODO: Ask informations about project name, version, template, author, license, dependencies, flags, etc.
      * TODO: Create the package.json file.
      * TODO: exit.
      **/
      /*if (FileUtils.test (Constants.PACKAGE_FILE, FileTest.EXISTS)) { // If we already have a package file, warn the user.
        Logger.logc (
          LogColor.YELLOW, "It seems that a %s file already exists in this folder, it will be overriden.",
          Constants.PACKAGE_FILE
        );
        Logger.logcc (LogColor.YELLOW, "Are you sure you want to proceed anyway? [Y/n] ");
        string proceed = stdin.read_line ();

        if (proceed.down () != "y") {
          Logger.logc (LogColor.RED, "Project creation aborted.");
          return;
        }
        
        print ("\n");
      }

      Logger.logc (LogColor.BLUE, "# Creating a new Vala project");
      Project proj = new Project ();

      // Ask for project name.
      Logger.logcc (LogColor.CYAN, "- Project name: ");
      proj.name = stdin.read_line ();

      // Ask for project version.
      Logger.logcc (LogColor.CYAN, "- Project version (0.1.0): ");
      string version = stdin.read_line ();
      if (version != "") {
        proj.version = version;
      } else {
        proj.version = "0.1.0";
      }

      // Ask for project author.
      Logger.logcc (LogColor.CYAN, "- Project author (Full name <mail@domain.tld>): ");
      proj.author = stdin.read_line ();

      // Ask for project license.
      Logger.logcc (LogColor.CYAN, "- Project license (MIT): ");
      string license = stdin.read_line ();
      if (license != "") {
        proj.license = license;
      } else {
        proj.license = "MIT";
      }

      // Ask for project repository.
      Logger.logcc (LogColor.CYAN, "- Git repository: ");
      string git_repo = stdin.read_line ();
      if (git_repo != "") {
        proj.repository = git_repo;
      }
      
      Logger.logc (LogColor.BLUE, "\n # Code generator options");
      
      // Ask for project vapis directory.
      Logger.logcc (LogColor.CYAN, "- VAPIs directory (src/vapis/): ");
      string vapis = stdin.read_line ();
      if (vapis != "") {
        proj.vapis_directory = vapis;
      } else {
        proj.vapis_directory = "src/vapis/";
      }
      
      Logger.logcc (LogColor.CYAN, "- Would you like to generate src/main.vala ? [Y/n] ");
      string generate_boilerplate = stdin.read_line ();
      if (generate_boilerplate.down () == "y") {
        proj.has_generated_main = true;
      }

      // Let's generate the project json.
      proj.generate_package_json ();
      
      string json = proj.get_pretty_json ();
      
      if (proj.has_generated_main == false) {
        Logger.log ("\n%s will generate the following package.json:", Constants.APP_NAME);
        Logger.log (json);
      } else {
        Logger.log ("\n%s will generate the following files:", Constants.APP_NAME);
        Logger.logc (LogColor.BLUE, " - package.json: ");
        Logger.log (json);
        Logger.logc (LogColor.BLUE, " - src/main.vala: ");
        Logger.log (generated_main_file);
      }

      // Ask the user whether or not it wants to write this file to disk.
      Logger.logcc (LogColor.GREEN, "\nAre you sure you want to proceed anyway? [Y/n] ");
      string proceed = stdin.read_line ();
      if (proceed.down () == "y" || proceed == "") {
        try {
          proj.save_package (this.working_dir);
          
          if (proj.has_generated_main) {
            proj.generate_base (this.working_dir, generated_main_file);
          }

          Logger.logc (LogColor.GREEN, "Project `%s` has been initialized successfuly.", proj.name);
          Logger.logc (
            LogColor.BLUE, "Type %s%s %s%s to build.",
            LogColor.YELLOW, this.arguments[0], PoulpAction.BUILD, LogColor.BLUE
          );
        } catch (Error err) {
          Logger.logc (LogColor.RED, "Generating package.json failed. Error: %s", err.message);
        }
      } else {
        Logger.logc (LogColor.RED, "Project creation aborted.");
        return;
      }*/
    }

    private void action_build () {
      /**
      * TODO: Collect the list of files to build.
      * TODO: Collect the list of libraries to use for building.
      * TODO: Collect the list of developer-defined arguments to pass to valac.
      * TODO: Launch the compilation and verify that no error happened, else warn the user and exit.
      * TODO: exit.
      **/
      if (FileUtils.test (Constants.PACKAGE_FILE, FileTest.EXISTS) == false) {
        Logger.logc (
          LogColor.BLUE, "%s %s file is missing, consider running %s%s %s%s then rerun this command.",
          Constants.APP_NAME, Constants.PACKAGE_FILE, LogColor.YELLOW, this.arguments[0], PoulpAction.INIT, LogColor.BLUE
        );
        return;
      }
      
      Project proj = new Project (this.working_dir);
      proj.compile ();
    }

    private void action_install () {
      /**
      * TODO: Install the built binary and resources on the user computer.
      * TODO: Display a report of what have been installed, and where.
      * TODO: exit.
      **/
    }

    private void action_help () {
      /**
      * TODO: Display poulp help or help of a poulp action.
      * TODO: exit.
      **/
      
      if (this.arguments.length < 2) {
        /**
        * TODO: Display global help.
        **/
      } else {
        string help_action = this.arguments[2];
        switch (help_action) {
          case PoulpAction.INIT:
            Logger.log (
              "$ %s %s: Launch the project creator of Poulp.",
              this.arguments[0], PoulpAction.INIT
            );
            break;
          case PoulpAction.BUILD:
            Logger.log (
              "$ %s %s [debug|release]: Build the current project from package.json file.",
              this.arguments[0], PoulpAction.BUILD
            );
            break;
          case PoulpAction.INSTALL:
            Logger.log (
              "$ %s %s: Build the current project from package.json file and install it on the user's computer.",
              this.arguments[0], PoulpAction.INSTALL
            );
            break;
          default:
            /*Logger.logc (
              LogColor.RED, "Unknown action.%s Type %s%s %s%s for a list of available actions.",
              LogColor.NORMAL, LogColor.YELLOW, this.arguments[0], PoulpAction.HELP, LogColor.NORMAL
            );*/
            this.action_usage ();
            break;
        }
      }
    }

    private void action_usage () {
      //if (this.arguments.length == 1) {
        Logger.log ("Usage: %s <action> [arguments]", this.arguments[0]);
        Logger.log ("Where <action> can be any of the following: init, build, install, help.\n");
        Logger.logc (
          LogColor.BLUE, "For detailed help about a specific action, type " + LogColor.YELLOW + "%s help <action>",
          this.arguments[0]
        );

        return;
      //}
    }

  }
}
