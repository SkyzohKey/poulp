namespace Poulp {
  public class Constants {
    public static const string APP_NAME = "Poulp";
    public static const string APP_VERSION = "0.1.1";
    public static const string PACKAGE_FILE = "package.json";
  }
}
