using Json;

namespace Poulp {
  public class PoulpApp : Application {

      // Couleurs pour les logs
      

      private string[] to_delete = new string[] {};

      public PoulpApp () {
          GLib.Object (application_id: "org.bat.poulp", flags: ApplicationFlags.HANDLES_COMMAND_LINE);
          set_inactivity_timeout (10);
      }

      public override void activate () {
          this.hold ();
          this.release ();
      }

      public override int command_line (ApplicationCommandLine command_line) {
          this.hold ();

          string[] args = command_line.get_arguments ();

          // On vérifie qu'on ait spécifié une action
          if (args.length == 1) {
            Logger.log ("Usage: poulp <init|build|gen|install|help>");
            Logger.logc (LogColor.BLUE, "For detailed help about a specific action, type " + LogColor.YELLOW.to_string () + "poulp help <action>");
            
            this.quit (); // Don't go further if missing arguments.
            return 1;
          }

          string action = args[1];

          switch (action) {
              case "init":
                  init (command_line.get_cwd ());
                  break;
              case "build":
                  build (command_line.get_cwd());
                  break;
              case "gen":
                  gen (command_line.get_cwd (), args[2], args[3]);
                  break;
              case "install":
                  install (args[2]);
                  break;

              default:
                  init (command_line.get_cwd ());
                  break;
          }

          this.release ();
          return 0;
      }

      // Merci StackOverFlow !
      public bool copy_recursive (GLib.File src, GLib.File dest, GLib.FileCopyFlags flags = GLib.FileCopyFlags.NONE, GLib.Cancellable? cancellable = null) throws GLib.Error {
          try {

              GLib.FileType src_type = src.query_file_type (GLib.FileQueryInfoFlags.NONE, cancellable);
              if ( src_type == GLib.FileType.DIRECTORY && !src.get_path ().contains ("/.git")) {
                  src.copy_attributes (dest, flags, cancellable);

                  string src_path = src.get_path ();
                  string dest_path = dest.get_path ();
                  GLib.FileEnumerator enumerator = src.enumerate_children (GLib.FileAttribute.STANDARD_NAME, GLib.FileQueryInfoFlags.NONE, cancellable);
                  for ( GLib.FileInfo? info = enumerator.next_file (cancellable) ; info != null ; info = enumerator.next_file (cancellable) ) {
                      copy_recursive (
                      GLib.File.new_for_path (GLib.Path.build_filename (src_path, info.get_name ())),
                      GLib.File.new_for_path (GLib.Path.build_filename (dest_path, info.get_name ())),
                      flags,
                      cancellable);
                  }
              } else if ( src_type == GLib.FileType.REGULAR ) {
                  print ("adding temp file : %s\n", dest.get_path ());
                  to_delete += dest.get_path ();
                  src.copy (dest, flags, cancellable);
              }

              return true;
          } catch (Error err) {
            Logger.logc (LogColor.RED, "Error: %s", err.message);
            return false;
          }
      }

      void init (string cwd) {
          print ("Création d'un nouveau projet ...\n");
          print ("Choisissez un nom pour votre projet : ");

          string name = stdin.read_line ();

          print ("Choisissez un modèle pour votre projet :\n");
          Gee.ArrayList<Project> prjs = new Gee.ArrayList<Project> ();

          try {
      		string directory = GLib.Path.build_filename(Environment.get_user_data_dir (), "poulp","projects");
      		Dir dir = Dir.open (directory);
      		string? file_name = null;
              int i = 0;

      		while ((file_name = dir.read_name ()) != null) {
                  // si c'est un fichier Tentacle
                  if (new Regex("\\.tentacle$").match(file_name)) {
                      string path = GLib.Path.build_filename (directory, file_name);
                      Project prj = read_project_file (path);
                      print ("%d - %s \n", i, prj.name);
                      prjs.add (prj);
                      i++;
                  }
      		}
      	} catch (Error err) {
      		stderr.printf (err.message);
      	}

          Project template = prjs [int.parse (stdin.read_line ())];

          File template_dir = File.new_for_path (GLib.Path.build_filename (Environment.get_user_data_dir (), "poulp", "projects", template.name));
          File dest_dir = File.new_for_path (cwd);

          print ("%s \n %s \n", template_dir.get_basename (), dest_dir.get_basename ());

          try {
              copy_recursive (template_dir, dest_dir, FileCopyFlags.OVERWRITE);
              print ("Projet correctement généré !");
          } catch (Error err) {
            Logger.logc (LogColor.RED, "Cannot create project, error: %s", err.message);
          }

          Builder builder = new Builder ();
          builder.begin_object ();
          builder.set_member_name ("name");
          builder.add_string_value (name);
          builder.set_member_name ("version");
          builder.add_string_value ("1.0.0");
          builder.set_member_name ("libs");
          builder.begin_array ();
          builder.add_string_value ("gio-2.0");
          builder.end_array ();
          builder.set_member_name ("output");
          builder.add_string_value (name);
          builder.end_object ();

          Generator generator = new Generator ();
          generator.root = builder.get_root ();

          try {
              generator.to_file ("package.json");
          } catch {
            Logger.logc (LogColor.RED, "Generating package.json failed.");
          }
      }

      void build (string cwd) {
          Parser parser = new Parser ();

          string[] cmd = { "valac" }; // TODO: Won't work, * isn't expanded by spawn...

          try {
              parser.load_from_file ("package.json");
              Json.Node root = parser.get_root ();
              Json.Array libs = root.get_object ().get_array_member ("libs");
              libs.foreach_element ((arr, index, elem) => {

                  if (Regex.match_simple ("^lib:", elem.get_string ())) {
                      print ("copie de %s en cours ...", elem.get_string ().substring (4));
                      string lib_path = GLib.Path.build_filename(Environment.get_user_data_dir (), "poulp", "lib", elem.get_string ().substring (4));
                      File lib_dir = File.new_for_path (lib_path);
                      File dest_dir = File.new_for_path (cwd);
                      copy_recursive (lib_dir, dest_dir, FileCopyFlags.OVERWRITE);
                  } else {
                      cmd += "--pkg " + elem.get_string ();
                  }
              });
              cmd += "--output " + root.get_object ().get_string_member ("output");
              
              // DEBUG ZONE:
              cmd += "logger.vala poulp.vala project.vala";
              
              //cmd += " -D WIN";
          } catch (Error err) {
            Logger.logc (LogColor.RED, "error: %s", err.message);
          }

          // Spawn de valac
          string valac_cmd = string.joinv (" ", cmd);
          string valac_stdout;
          string valac_stderr;
          int valac_status;

          try {
              Logger.logc (LogColor.GREEN, "Running %s", valac_cmd);
              Process.spawn_command_line_sync (valac_cmd, out valac_stdout, out valac_stderr, out valac_status);

              if (valac_status == 0) {
                Logger.logc (LogColor.GREEN, "Compilation succeeded!");
              } else {
                Logger.logc (LogColor.RED, valac_stdout);
                Logger.log (valac_stderr);
              }

          } catch (SpawnError err) {
            Logger.logc (LogColor.RED, "Cannot build project, error: %s", err.message);
          }

          foreach (string to_del in this.to_delete) {
              print ("deleting : %s\n", to_del);
              File file_to_del = File.new_for_path (to_del);
              file_to_del.delete ();
          }
      }

      void gen (string cwd, string template, string? name = null) {

          if (name == null) {
              name = template;
          }

          string file_path = GLib.Path.build_filename(Environment.get_user_data_dir (), "poulp", "files", template);
          File file = File.new_for_path (file_path);
          File dest = File.new_for_path (GLib.Path.build_filename (cwd, name));
          file.copy (dest, 0);
      }

      void install (string url) {
          string[] args = {"git", "clone", url};
          string poulp_dir = GLib.Path.build_filename(Environment.get_user_data_dir (), "poulp", "lib");
          int git_err;


          try {
              Process.spawn_sync (poulp_dir, args, Environ.get (), SpawnFlags.SEARCH_PATH, null, null, null, out git_err);
          } catch (Error err) {
              print ("Erreur lors de la copie du depot git. Assurez vous d'avoir git.\n%s", err.message);
          }
      }

      Project read_project_file (string fileName) {
          Project prj = new Project ();

          // Merci libxml, avec tout ces -> j'ai l'impression de faire du C++ ...

          Xml.Doc* doc = Xml.Parser.parse_file (fileName);
          Xml.Node* root = doc->get_root_element ();
          prj.name = root->get_prop ("Name");

          for (Xml.Node* iter = root->children; iter != null; iter = iter->next) {
              switch (iter->name) {
                  case "Libs":
                      for (Xml.Node* lib_iter = iter->children; lib_iter != null; lib_iter = lib_iter->next) {
                          prj.libs.add (lib_iter->get_prop ("Name"));
                      }
                      break;
                  case "Launch":
                      for (Xml.Node* launch_iter = iter->children; launch_iter != null; launch_iter = launch_iter->next) {
                          prj.launch.add (launch_iter->get_prop ("Value"));
                      }
                      break;
              }
          }

          return prj;

      }
}

    public static int main (string[] args) {
        return new PoulpApp ().run (args);
    }

}
