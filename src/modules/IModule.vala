using GLib;

namespace Poulp.Modules {

  public class IModule : Object {
    public string module_name { get; set; default = "Unknown"; }
    public string module_description { get; set; default = "An unknown module"; }
    public string working_dir { get; set; default = null; }
    public string[] arguments { get; set; default = {}; }

    public IModule (string[] args, string cwd, string name, string desc) {
      this.arguments = args;
      this.working_dir = cwd;
      this.module_name = name;
      this.module_description = desc;

      Logger.logc (LogColor.BLUE, "%s - %s", this.module_name, this.module_description);

      if (this.perform_checks ()) {
        if (this.load_module ()) {
          this.unload_module ();
        }
      }
    }

    public virtual bool perform_checks () {
      return false;
      //throw new ModuleError.UNIMPLEMENTED_METHOD ("Unimplemented method perform_checks.");
    }

    public virtual bool load_module () {
      return false;
      //throw new ModuleError.UNIMPLEMENTED_METHOD ("Unimplemented method load_module.");
    }

    public virtual void unload_module () {
      //throw new ModuleError.UNIMPLEMENTED_METHOD ("Unimplemented method unload_module.");
    }
  }

}
