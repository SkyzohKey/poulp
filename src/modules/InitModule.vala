using GLib;

namespace Poulp.Modules {

  public class InitModule : IModule {
    public InitModule (string[] args, string cwd) {
      base (args, cwd, PoulpAction.INIT, "A command to create a new Vala project using %s".printf (Constants.APP_NAME));
    }
    
    public override bool perform_checks () {
      Logger.log ("Current working directory: %s", this.working_dir);
      
      string package_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, this.working_dir, Constants.PACKAGE_FILE);
      if (FileUtils.test (package_path, FileTest.EXISTS)) { // If we already have a package file, warn the user.
        Logger.logc (
          LogColor.YELLOW, "It seems that a %s file already exists in this folder, it will be overriden.",
          Constants.PACKAGE_FILE
        );
        Logger.logcc (LogColor.YELLOW, "Are you sure you want to proceed anyway? [Y/n] ");
        string proceed = stdin.read_line ();

        if (proceed.down () != "y") {
          Logger.logc (LogColor.RED, "Project creation aborted.");
          return false;
        }
      }
      
      return true;
    }
    
    public override bool load_module () {
      Logger.logc (LogColor.BLUE, "# Creating a new Vala project");
      Project proj = new Project (this.working_dir);

      // Ask for project name.
      Logger.logcc (LogColor.CYAN, "- Project name: ");
      proj.name = stdin.read_line ();

      // Ask for project version.
      Logger.logcc (LogColor.CYAN, "- Project version (0.1.0): ");
      string version = stdin.read_line ();
      if (version != "") {
        proj.version = version;
      } else {
        proj.version = "0.1.0";
      }

      // Ask for project author.
      Logger.logcc (LogColor.CYAN, "- Project author (Full name <mail@domain.tld>): ");
      proj.author = stdin.read_line ();

      // Ask for project license.
      Logger.logcc (LogColor.CYAN, "- Project license (MIT): ");
      string license = stdin.read_line ();
      if (license != "") {
        proj.license = license;
      } else {
        proj.license = "MIT";
      }

      // Ask for project repository.
      Logger.logcc (LogColor.CYAN, "- Git repository: ");
      string git_repo = stdin.read_line ();
      if (git_repo != "") {
        proj.repository = git_repo;
      }
      
      Logger.logc (LogColor.BLUE, "\n # Code generator options");
      
      // Ask for project vapis directory.
      Logger.logcc (LogColor.CYAN, "- VAPIs directory (src/vapis/): ");
      string vapis = stdin.read_line ();
      if (vapis != "") {
        proj.vapis_directory = vapis;
      } else {
        proj.vapis_directory = "src/vapis/";
      }
      
      Logger.logcc (LogColor.CYAN, "- Would you like to generate src/main.vala ? [Y/n] ");
      string generate_boilerplate = stdin.read_line ();
      if (generate_boilerplate.down () == "y") {
        proj.has_generated_main = true;
      }

      // Let's generate the project json.
      proj.generate_package_json ();
      string json = proj.get_pretty_json ();
      
      if (proj.has_generated_main == false) {
        Logger.log ("\n%s will generate the following package.json:", Constants.APP_NAME);
        Logger.log (json);
      } else {
        Logger.log ("\n%s will generate the following files:", Constants.APP_NAME);
        Logger.logc (LogColor.BLUE, " - package.json: ");
        Logger.log (json);
        Logger.logc (LogColor.BLUE, " - src/main.vala: ");
        Logger.log (PoulpApplication.generated_main_file);
      }

      // Ask the user whether or not it wants to write this file to disk.
      Logger.logcc (LogColor.GREEN, "\nAre you sure you want to proceed anyway? [Y/n] ");
      string proceed = stdin.read_line ();
      if (proceed.down () == "y" || proceed == "") {
        try {
          proj.save_package (this.working_dir);
          
          if (proj.has_generated_main) {
            proj.generate_base (PoulpApplication.generated_main_file);
          }

          Logger.logc (LogColor.GREEN, "Project `%s` has been initialized successfuly.", proj.name);
          Logger.logc (
            LogColor.BLUE, "Type %s%s %s%s to build.",
            LogColor.YELLOW, this.arguments[0], PoulpAction.BUILD, LogColor.BLUE
          );
        } catch (Error err) {
          Logger.logc (LogColor.RED, "Generating package.json failed. Error: %s", err.message);
        }
      } else {
        Logger.logc (LogColor.RED, "Project creation aborted.");
      }
      
      return true;
    }
    
    public override void unload_module () {
      /**
      * TODO: Unload the module here.
      **/
      return;
    }
  }

}
