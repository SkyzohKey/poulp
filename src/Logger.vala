namespace Poulp {
  /**
  * @enum LogColor - Defines the allowed colors.
  **/  
  public class LogColor {
    public static const string NORMAL  = "\x1B[0m";
    public static const string WHITE   = "\x1B[37m";
    public static const string GREEN   = "\x1B[32m";
    public static const string BLUE    = "\x1B[34m";
    public static const string YELLOW  = "\x1B[33m";
    public static const string MAGENTA = "\x1B[35m";
    public static const string CYAN    = "\x1B[36m";
    public static const string RED     = "\x1B[31m";
  }
  
  /**
  * @enum LogColor - Defines the allowed styles.
  **/  
  public class LogStyle {
    public static const string  BOLD = "\x1B[1m";
  }

  public class Logger : Object {
    /**
    * This function prints a message with full colored line.
    * @param {LogColor} color - The color to apply to the message.
    * @param {string} message - The message to display in logs.
    * @param {va_list} args - A va_list that'll be vsprintf'ed to message.
    **/
    public static void logc (string color, string message, ...) {
      stdout.puts (LogStyle.BOLD + color + message.vprintf (va_list ()) + LogColor.NORMAL + "\n");
    }
    
    /**
    * This function do the same as logc, but avoid adding a newline (\n) at the end of the message.
    * @param {LogColor} color - The color to apply to the message.
    * @param {string} message - The message to display in logs.
    * @param {va_list} args - A va_list that'll be vsprintf'ed to message.
    **/
    public static void logcc (string color, string message, ...) {
      stdout.puts (LogStyle.BOLD + color + message.vprintf (va_list ()) + LogColor.NORMAL);
    }
    
    /**
    * This function prints a message.
    * @param {string} message - The message to display in logs.
    * @param {va_list} args - A va_list that'll be vsprintf'ed to message.
    **/
    public static void log (string message, ...) {
      stdout.puts (message.vprintf (va_list ()) + "\n");
    }
  }
}
