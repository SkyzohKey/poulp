/**
* TODO: Licensing the code.
**/

using Gee;
using Json;

namespace Poulp {
  public class Project : GLib.Object {
    /**
    * {string} name - The poulp project name. Must be lower case with no spaces.
    **/
    public string name { get; set; default = "poulp-project"; }

    /**
    * {string} version - The poulp project version. Semver format is prefered but anything works for now.
    **/
    public string version { get; set; default = "0.1.0"; }

    /**
    * {string} author - The poulp project author. "Full Name <name@domain.tld>" format is prefered.
    **/
    public string author { get; set; default = "Full Name <name@domain.tld>"; }

    /**
    * {string} license - The poulp project license.
    **/
    public string license { get; set; default = "MIT"; }

    /**
    * {string} repository - The poulp project Git repository URL.
    **/
    public string repository { get; set; default = null; }
    
    /**
    * {string} vapis_directory - The project vapi's directory.
    **/
    public string vapis_directory { get; set; default = null; }

    /**
    * {Gee.ArrayList<string>} libs - A list of libraries needed to compile the poulp project.
    **/
    public ArrayList<string> libs { get; set; default = new ArrayList<string> (); }

    /**
    * {Gee.ArrayList<string>} src_files - TODO: A list of the source files that needs to be compiled.
    **/
    public ArrayList<string> src_files { get; set; default = new ArrayList<string> (); }

    public bool has_generated_main = false;

    private Builder builder = new Builder ();
    private Generator generator ;
    private Parser parser = new Parser ();
    private string cwd = "";

    public Project (string cwd) {
      this.cwd = cwd;
    
      this.name = "poulp-project";
      this.version = "0.1.0";
      this.author = "Full Name <name@domain.tld>";
      this.license = "MIT";
    }

    public void compile () {
      try {
        string build_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, this.cwd, "build");
        DirUtils.create_with_parents (build_path, 0755);
        
        string package_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, this.cwd, Constants.PACKAGE_FILE);
        this.parser.load_from_file (package_path);

        Json.Node root = parser.get_root ();
        Json.Array files = root.get_object ().get_array_member ("files");
        Json.Array libs = root.get_object ().get_array_member ("dependencies");

        this.name = root.get_object ().get_string_member ("name");
        this.version = root.get_object ().get_string_member ("version");
        this.vapis_directory = root.get_object ().get_string_member ("vapis-dir");

        string name = this.name.down ().replace (" ", "-");
        Logger.logc (LogColor.BLUE, "# Building the project `%s`", name);
        string output_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, this.cwd, "build", @"$name-$(this.version)");
        string[] valac_args = {
          "valac",
          @"--output $output_path"
        };

        // Append packages.
        libs.foreach_element ((arr, index, elem) => {
          valac_args += "--pkg " + elem.get_string ();
        });

        // Append files.
        files.foreach_element ((arr, index, elem) => {
          valac_args += elem.get_string ();
        });
        
        string valac_cmd = string.joinv (" ", valac_args);
        string valac_stdout;
        string valac_stderr;
        int valac_status;
        
        Logger.logc (LogColor.GREEN, "Running %s", valac_cmd);
        Process.spawn_command_line_sync (valac_cmd, out valac_stdout, out valac_stderr, out valac_status);

        if (valac_status == 0) {
          Logger.logc (LogColor.GREEN, "Compilation succeeded!");
        } else {
          Logger.logc (LogColor.RED, valac_stdout);
          Logger.log (valac_stderr);
          Logger.logc (LogColor.RED, valac_stdout);
        }
      } catch (Error err) {
        Logger.logc (LogColor.RED, "Cannot build project, error: %s", err.message);
      }
    }
    
    public void generate_base (string file_template) throws Error {
      string source_dir = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, this.cwd, "src");
    
      if (FileUtils.test (source_dir, FileTest.EXISTS)) { // If we already have a package file, warn the user.
        Logger.logc (
          LogColor.YELLOW, "It seems that a src/ directory already exists, continuing will override it.",
          Constants.PACKAGE_FILE
        );
        Logger.logcc (LogColor.YELLOW, "Are you sure you want to proceed anyway? [Y/n] ");
        string proceed = stdin.read_line ();

        if (proceed.down () != "y") {
          Logger.logc (LogColor.RED, "src/ directory creation aborted.");
          return;
        }
        
        print ("\n");
      }
      
      DirUtils.create_with_parents (source_dir, 0755);
      
      string main_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, source_dir, "main.vala");
      FileUtils.set_data (main_path, file_template.data);
    }
    
    public void generate_package_json () {
      // Wow, that's such ugly, lel.
      // Let's generate the JSON object.
      this.builder.begin_object ();

      this.builder.set_member_name ("name");
      this.builder.add_string_value (this.name);

      this.builder.set_member_name ("version");
      this.builder.add_string_value (this.version);

      this.builder.set_member_name ("author");
      this.builder.add_string_value (this.author);

      this.builder.set_member_name ("license");
      this.builder.add_string_value (this.license);
      
      if (this.repository != null && this.repository != "") {
        this.builder.set_member_name ("repository");
        this.builder.add_string_value (this.repository);
      }

      if (this.vapis_directory != null && this.vapis_directory != "") {
        this.builder.set_member_name ("vapis-dir");
        this.builder.add_string_value (this.vapis_directory);
      }

      this.builder.set_member_name ("dependencies");
      this.builder.begin_array ();
        this.builder.add_string_value ("glib-2.0");
      this.builder.end_array ();
      
      this.builder.set_member_name ("files");
      this.builder.begin_array ();
      
      // Append a list of available files in src/
      string source_dir = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, this.cwd, "src");
      GLib.Array<string> files = this.list_files (true, source_dir);
      
      this.builder.end_array ();
      this.builder.end_object ();

      this.generator = new Generator ();
      this.generator.pretty = true;
      Json.Node root = builder.get_root ();
      this.generator.set_root (root);
    }
    
    public string get_pretty_json () {
      return this.generator.to_data (null);
    }
    
    public void save_package (string cwd) throws Error {
      string path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, cwd, Constants.PACKAGE_FILE);    
      this.generator.to_file (path);
    }
    
    public GLib.Array<string> list_files (bool append_builder = false, string path, string parent_dir = "") {
      File file = File.new_for_path (path);
      FileEnumerator files = file.enumerate_children ("standard::*", FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null);
      FileInfo info = null;
      
      GLib.Array<string> source_files = new GLib.Array<string> ();
      while ((info = files.next_file (null)) != null) {
        if (info.get_file_type () == FileType.DIRECTORY) {
          File subdir = file.resolve_relative_path (info.get_name ());
          this.list_files (append_builder, subdir.get_path (), info.get_name ());
        } else {
          string file_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, "src", parent_dir, info.get_name ());
          
          if (file_path.has_suffix (".vala") || file_path.has_suffix (".gs") || file_path.has_suffix (".c")) {
            //stdout.printf (file_path + "\n");
            source_files.append_val (file_path);
          }
        }
      }
      
      if (append_builder) {
        for (int i = 0; i < source_files.length; i++) {
          string f = source_files.index (i);
          builder.add_string_value (f);
        }
      }
      
      //print (string.joinv (", ", source_files));
      return source_files;
    }
  }
}
