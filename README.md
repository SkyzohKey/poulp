# poulp

poulp is a little programm that'll help you generating files or projects, compiling your app, or installing librairies.

# Usage

First, create a `package.json` file;

```json
{
  "name": "my-app",
  "version": "1.4.6",
  "libs": [
    "gtk+-3.0",
    "gio-2.0",
    "libsoup-2.4"
  ]
}
```

Then, open a terminal and just run the following command.

```
poulp build
```

You're done !

# TODO

- Use the same system as ValaWinPkg for install dependencies